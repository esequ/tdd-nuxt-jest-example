import { render, screen } from '@testing-library/vue'
import money from '~/components/Datatable/cell/money'
import { validateNumber } from '~/utils/validations'

describe('Money', () => {
  it('Validate correct value object', () => {
    expect(() => validateNumber({ value: 123 })).not.toThrow()
  })
  it('Validate incorrect value object', () => {
    expect(() => validateNumber({ value: 'fas' })).toThrow()
  })
  it('Render correctly money component', () => {
    render(money, {
      props: {
        name: 'money',
        value: { value: 100 }
      }
    })
    screen.getByText('$100.00')
  })
})

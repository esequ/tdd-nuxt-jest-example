export function downloadFileXls(response) {
  if (!response.headers['content-disposition']) {
    throw ContentDispositionUnexist('Check headers exposed')
  }
  const fileName = response.headers['content-disposition'].split('filename=')[1]
  if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE variant
    window.navigator.msSaveOrOpenBlob(new Blob([response.data],
      { type: 'application/vnd.ms-excel' }),
    fileName)
  } else {
    const url = window.URL.createObjectURL(new Blob([response.data],
      { type: 'application/vnd.ms-excel' }))
    const link = document.createElement('a')
    link.href = url
    link.setAttribute('download',
      response.headers['content-disposition'].split('filename=')[1])
    document.body.appendChild(link)
    link.click()
    return true
  }
}

class ContentDispositionUnexist extends Error {
  constructor (message) {
    super(message)
    this.name = 'Cant access to content disposition header'
    this.message = message
  }
}

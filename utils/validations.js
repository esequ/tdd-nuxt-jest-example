export function validateNumber (variant) {
  if (!variant.value || isNaN(variant.value)) {
    throw new Error(
      'value must be ' +
      'A number ' +
      `You passed:${variant.value}`
    )
  }
  return true
}
